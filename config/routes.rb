Rails.application.routes.draw do
=begin namespace :api do
    namespace :v1 do
      get 'articles/index'
    end
  end

  namespace :api do
    namespace :v1 do
      get 'articles/show'
    end
  end

  namespace :api do
    namespace :v1 do
      get 'articles/create'
    end
  end

  namespace :api do
    namespace :v1 do
      get 'articles/destroy'
    end
  end

  namespace :api do
    namespace :v1 do
      get 'articles/update'
    end
  end
=end
  namespace 'api' do
    namespace 'v1' do
          resources :articles
          resources :users
    end
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
